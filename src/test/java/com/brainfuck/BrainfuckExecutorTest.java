package com.brainfuck;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
@RunWith(MockitoJUnitRunner.class)
public class BrainfuckExecutorTest {

    private static final String TEST_STRING = "+>";
    private static final int PLUS_COMMAND_INDEX = 0;
    private static final int MOVE_POINTER_SYMBOL_INDEX = 1;

    @Mock
    private Map<String, ICommand> commandExecutors;

    @Mock
    private ICommand increaseCellValueCommand;
    @Mock
    private ICommand increasePointerValueCommand;
    @Mock
    private BrainfuckEnvironment environment;

    private BrainfuckExecutor executor;

    @Before
    public void setUp() {
        when(commandExecutors.get("+")).thenReturn(increaseCellValueCommand);
        when(commandExecutors.get(">")).thenReturn(increasePointerValueCommand);
        when(increaseCellValueCommand.execute(environment, PLUS_COMMAND_INDEX)).thenReturn(PLUS_COMMAND_INDEX);
        when(increasePointerValueCommand.execute(environment, MOVE_POINTER_SYMBOL_INDEX)).thenReturn(MOVE_POINTER_SYMBOL_INDEX);
        executor = new BrainfuckExecutor(environment, commandExecutors);
    }

    @Test
    public void shouldInvokeExecuteMethodOnHandlers() {
        executor.execute(TEST_STRING);
        verify(increaseCellValueCommand).execute(environment, PLUS_COMMAND_INDEX);
        verify(increasePointerValueCommand).execute(environment, MOVE_POINTER_SYMBOL_INDEX);
    }
}