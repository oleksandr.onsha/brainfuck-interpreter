package com.brainfuck.command.impl.loop;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import com.brainfuck.environment.Loop;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.InvalidParameterException;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
public class LoopStartCommandTest {

    private static final int CODE_INDEX = 0;

    private BrainfuckEnvironment environment;
    private ICommand loopStartCommand;

    @Before
    public void setUp() {
        environment = new BrainfuckEnvironment(2);
        loopStartCommand = new LoopStartCommand();
    }

    @Test
    public void shouldAddLoopToEnvironment() {
        environment.incrementPointer();
        Loop expectedLoop = new Loop(CODE_INDEX, 1);

        loopStartCommand.execute(environment, CODE_INDEX);
        Loop actualLoop = environment.getPreviousLoop();

        Assert.assertEquals(expectedLoop.getStartIndex(), actualLoop.getStartIndex());
    }

    @Test(expected = InvalidParameterException.class)
    public void shouldThrowAnExceptionIfEnvironmentIsNull() {
        loopStartCommand.execute(null, 0);
    }
}