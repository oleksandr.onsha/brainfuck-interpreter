package com.brainfuck.command.impl.loop;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import com.brainfuck.environment.Loop;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
public class LoopEndCommandTest {

    private static final int CODE_INDEX = 2;
    private static final int LOOP_START_INDEX = 0;

    private BrainfuckEnvironment environment;
    private Loop loop;
    private ICommand loopEndCommand;

    @Before
    public void setUp() {
        environment = new BrainfuckEnvironment(2);
        loop = new Loop(LOOP_START_INDEX, 0);
        loopEndCommand = new LoopEndCommand();
    }

    @Test
    public void shouldReturnLoopStartIndex() {
        environment.addLoop(loop);
        environment.setCurrentCellValue((byte)1);
        int commandResult = loopEndCommand.execute(environment, CODE_INDEX);
        Assert.assertEquals(CODE_INDEX, loop.getEndIndex());
        Assert.assertEquals(commandResult, LOOP_START_INDEX);
    }

    @Test(expected = RuntimeException.class)
    public void shouldRemoveLoopAndReturnCodeIndex() {
        environment.addLoop(loop);
        int commandResult = loopEndCommand.execute(environment, CODE_INDEX);
        Assert.assertEquals(CODE_INDEX, loop.getEndIndex());
        Assert.assertEquals(commandResult, CODE_INDEX);
        environment.getPreviousLoop();
    }
}