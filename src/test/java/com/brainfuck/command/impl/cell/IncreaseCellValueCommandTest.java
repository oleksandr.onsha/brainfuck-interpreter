package com.brainfuck.command.impl.cell;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.InvalidParameterException;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
@RunWith(MockitoJUnitRunner.class)
public class IncreaseCellValueCommandTest {

    private static final int CODE_INDEX = 0;
    private static final int EXPECTED_CELL_VALUE = 1;

    private BrainfuckEnvironment environment;
    private ICommand increaseCellValueCommand;

    @Before
    public void setUp() {
        environment = new BrainfuckEnvironment(1);
        increaseCellValueCommand = new IncreaseCellValueCommand();
    }

    @Test
    public void shouldIncreaseCellValue() {
        increaseCellValueCommand.execute(environment, CODE_INDEX);
        Assert.assertEquals(EXPECTED_CELL_VALUE, environment.getCurrentCellValue());
    }

    @Test(expected = InvalidParameterException.class)
    public void shouldThrowAnExceptionIfEnvironmentIsNull() {
        increaseCellValueCommand.execute(null, CODE_INDEX);
    }

}