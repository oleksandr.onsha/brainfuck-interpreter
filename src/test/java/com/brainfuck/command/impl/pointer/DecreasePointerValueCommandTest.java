package com.brainfuck.command.impl.pointer;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
public class DecreasePointerValueCommandTest {

    private static final int CODE_INDEX = 0;
    private BrainfuckEnvironment environment;
    private ICommand decreasePointerValueCommand;

    @Before
    public void setUp() {
        environment = new BrainfuckEnvironment(2);
        decreasePointerValueCommand = new DecreasePointerValueCommand();
    }

    @Test
    public void shouldDecreasePointerValueCommand() {
        environment.incrementPointer();
        int result = decreasePointerValueCommand.execute(environment, CODE_INDEX);
        Assert.assertEquals(result, CODE_INDEX);
        Assert.assertEquals(0, environment.getCurrentPointer());
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionDueToIndexOutOfArrayBounds() {
        environment.decrementPointer();
    }

}