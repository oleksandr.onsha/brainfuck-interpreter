package com.brainfuck;

import com.brainfuck.command.ICommand;
import com.brainfuck.command.impl.PrintCommand;
import com.brainfuck.command.impl.cell.DecreaseCellValueCommand;
import com.brainfuck.command.impl.cell.IncreaseCellValueCommand;
import com.brainfuck.command.impl.loop.LoopEndCommand;
import com.brainfuck.command.impl.loop.LoopStartCommand;
import com.brainfuck.command.impl.pointer.DecreasePointerValueCommand;
import com.brainfuck.command.impl.pointer.IncreasePointerValueCommand;
import com.brainfuck.environment.BrainfuckEnvironment;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Application entry point.
 *
 * Created by Oleksandr Onsha on 9/22/18
 */
public class BrainfuckConsoleInterpreter {

    private static Map<String, ICommand> commandExecutors = new HashMap<>();
    private static BrainfuckExecutor executor;

    static {
        commandExecutors.put(">", new IncreasePointerValueCommand());
        commandExecutors.put("<", new DecreasePointerValueCommand());
        commandExecutors.put("+", new IncreaseCellValueCommand());
        commandExecutors.put("-", new DecreaseCellValueCommand());
        commandExecutors.put(".", new PrintCommand());
        commandExecutors.put("[", new LoopStartCommand());
        commandExecutors.put("]", new LoopEndCommand());

        executor = new BrainfuckExecutor(new BrainfuckEnvironment(), commandExecutors);

    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String brainfuckCode = askBrainfuckCode(s);
        while (!brainfuckCode.equals("exit")) {
            System.out.println("The result is:");
            executor.execute(brainfuckCode);
            System.out.println();
            brainfuckCode = askBrainfuckCode(s);
            executor.resetEnvironment();
        }
    }

    private static String askBrainfuckCode(Scanner s) {
        System.out.println("Enter Brainfuck code [or 'exit']: ");
        return s.nextLine();
    }
}
