package com.brainfuck.command;

import com.brainfuck.environment.BrainfuckEnvironment;

/**
 * Command interface.
 */
public interface ICommand {

    /**
     * Executes brainfuck commands on the environment.
     *
     * @param environment is the memory container where commands are executed.
     * @return index to continue Brainfuck string processing.
     */
    int execute(BrainfuckEnvironment environment, int codeIndex);

}
