package com.brainfuck.command.impl.pointer;

import com.brainfuck.command.impl.AbstractCommand;
import com.brainfuck.environment.BrainfuckEnvironment;

/**
 * Increase pointer value command.
 */
public class IncreasePointerValueCommand extends AbstractCommand {

    /**
     * Increments value of the environment pointer by 1.
     *
     * @param environment is the memory container where commands are executed.
     * @param codeIndex   current index of the Brainfuck code string.
     * @return not modified codeIndex.
     */
    @Override
    public int executeCommand(BrainfuckEnvironment environment, int codeIndex) {
        environment.incrementPointer();
        return codeIndex;
    }
}
