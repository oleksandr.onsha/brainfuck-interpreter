package com.brainfuck.command.impl;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;

import java.security.InvalidParameterException;

/**
 * Created by Oleksandr Onsha on 9/22/18
 */
public abstract class AbstractCommand implements ICommand {

    @Override
    public int execute(BrainfuckEnvironment environment, int codeIndex) {
        if (environment == null) {
            throw new InvalidParameterException("Environment must not be null");
        }
        return executeCommand(environment, codeIndex);
    }

    protected abstract int executeCommand(BrainfuckEnvironment environment, int codeIndex);
}
