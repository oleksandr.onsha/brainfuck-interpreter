package com.brainfuck.command.impl.cell;

import com.brainfuck.command.impl.AbstractCommand;
import com.brainfuck.environment.BrainfuckEnvironment;

/**
 * Decrease cell value command.
 */
public class DecreaseCellValueCommand extends AbstractCommand {

    /**
     * Decreases value of the current cell by 1.
     *
     * @param environment is the memory container where commands are executed.
     * @param codeIndex   current index of the Brainfuck code string.
     * @return not modified codeIndex.
     */
    @Override
    public int executeCommand(BrainfuckEnvironment environment, int codeIndex) {
        environment.decrementCurrentCellValue();
        return codeIndex;
    }
}
