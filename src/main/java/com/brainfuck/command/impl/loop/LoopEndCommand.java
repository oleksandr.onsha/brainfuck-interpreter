package com.brainfuck.command.impl.loop;

import com.brainfuck.command.impl.AbstractCommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import com.brainfuck.environment.Loop;

/**
 * Loop end command.
 */
public class LoopEndCommand extends AbstractCommand {

    /**
     * Sets end index to the current loop.
     * If the value of loop counter is equal to the '0'
     * the loop is removed from the BrainfuckEnvironment.
     *
     * @param environment is the memory container where commands are executed.
     * @param codeIndex   current index of the Brainfuck code string.
     * @return if counter value is equal to '0' method will return the loop start index.
     * Otherwise not modified codeIndex will be returned.
     */
    @Override
    public int executeCommand(BrainfuckEnvironment environment, int codeIndex) {
        Loop previousLoop = environment.getPreviousLoop();
        previousLoop.setEndIndex(codeIndex);
        int counterIndex = previousLoop.getEnvironmentCounterPointer();
        if (environment.getCellValue(counterIndex) != 0) {
            return previousLoop.getStartIndex();
        } else {
            environment.removeLastLoop();
            return codeIndex;
        }
    }
}
