package com.brainfuck.command.impl.loop;

import com.brainfuck.command.impl.AbstractCommand;
import com.brainfuck.environment.BrainfuckEnvironment;
import com.brainfuck.environment.Loop;

/**
 * Loop start command.
 */
public class LoopStartCommand extends AbstractCommand {

    /**
     * Creates and adds new loop to the passed BrainfuckEnvironment.
     *
     * @param environment is the memory container where commands are executed.
     * @param codeIndex   current index of the Brainfuck code string.
     * @return not modified codeIndex.
     */
    @Override
    public int executeCommand(BrainfuckEnvironment environment, int codeIndex) {
        Loop loop = new Loop(codeIndex, environment.getCurrentPointer());
        environment.addLoop(loop);
        return codeIndex;
    }
}
