package com.brainfuck.command.impl;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;

/**
 * Print command.
 * Converts int to char and prints it using System.out.print() method.
 */
public class PrintCommand implements ICommand {

    /**
     * Converts the value from the current cell from int to char and prints it using System.out.print() method.
     *
     * @param environment is the memory container where commands are executed.
     * @param codeIndex   current index of the Brainfuck code string.
     * @return not modified codeIndex.
     */
    @Override
    public int execute(BrainfuckEnvironment environment, int codeIndex) {
        int currentPointer = environment.getCurrentPointer();
        int cellValue = environment.getCellValue(currentPointer);
        System.out.print((char) cellValue);
        return codeIndex;
    }
}
