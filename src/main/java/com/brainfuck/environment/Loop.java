package com.brainfuck.environment;

/**
 * Represents loop description.
 */
public class Loop {

    /**
     * Index of the loop start symbol from the Brainfuck code string.
     */
    private int startIndex;

    /**
     * Index of the loop end symbol from the Brainfuck code string.
     */
    private int endIndex;

    /**
     * Index of the loop counter cell from the BrainfuckEnvironment.
     */
    private int environmentCounterPointer;

    public Loop(int codeStartIndex, int environmentCounterPointer) {
        this.startIndex = codeStartIndex;
        this.environmentCounterPointer = environmentCounterPointer;
        this.endIndex = -1;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getEnvironmentCounterPointer() {
        return environmentCounterPointer;
    }
}
