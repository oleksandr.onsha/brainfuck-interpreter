package com.brainfuck.environment;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * Represents Brainfuck environment.
 */
public class BrainfuckEnvironment {

    /**
     * The size of the classical Brainfuck byte array suggested by Urban Müller.
     */
    private static final int DEFAULT_ENVIRONMENT_SIZE = 30_000;

    /**
     * Array where Brainfuck commands are executed.
     */
    private byte[] array;

    /**
     * Pointer to the current cell of the array.
     */
    private int currentPointer = 0;

    /**
     * List of the loops.
     */
    private Deque<Loop> loops;

    /**
     * Builds BrainfuckEnvironment with the custom array size;
     *
     * @param environmentSize size of the array;
     */
    public BrainfuckEnvironment(int environmentSize) {
        this.array = new byte[environmentSize];
        this.loops = new ArrayDeque<>();
    }

    /**
     * Builds BrainfuckEnvironment with the DEFAULT_ENVIRONMENT_SIZE;
     */
    public BrainfuckEnvironment() {
        this(DEFAULT_ENVIRONMENT_SIZE);
    }


    public void incrementPointer() {
        if (currentPointer == array.length - 1) {
            throw new RuntimeException(String.format("Environment array is %s in length, " +
                    "pointer must not point to the non-existent cells", array.length));
        }
        this.currentPointer++;
    }


    public void decrementPointer() {
        if (currentPointer == 0) {
            throw new RuntimeException("Pointer must not point to the cell with negative index");
        }
        this.currentPointer--;
    }


    public int getCurrentPointer() {
        return currentPointer;
    }

    public void incrementCurrentCellValue() {
        byte cellValue = array[currentPointer];
        cellValue++;
        array[currentPointer] = cellValue;
    }

    public void decrementCurrentCellValue() {
        byte cellValue = array[currentPointer];
        cellValue--;
        array[currentPointer] = cellValue;
    }

    public void addLoop(Loop loop) {
        this.loops.addLast(loop);
    }

    public void removeLastLoop() {
        loops.removeLast();
    }

    public Loop getPreviousLoop() {
        return loops.getLast();
    }

    public byte getCellValue(int cellIndex) {
        return array[cellIndex];
    }

    public byte getCurrentCellValue() {
        return array[currentPointer];
    }

    public void setCurrentCellValue(byte value) {
        array[currentPointer] = value;
    }

    public void resetEnvironment() {
        array = new byte[array.length];
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
