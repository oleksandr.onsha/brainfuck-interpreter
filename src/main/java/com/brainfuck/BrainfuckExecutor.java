package com.brainfuck;

import com.brainfuck.command.ICommand;
import com.brainfuck.environment.BrainfuckEnvironment;

import java.util.Map;

/**
 * Brainfuck executor.
 */
public class BrainfuckExecutor {

    private BrainfuckEnvironment environment;
    private Map<String, ICommand> commandParsers;

    /**
     * Custom Brainfuck environment builder.
     *
     * @param environment    traditional Brainfuck environment.
     * @param commandParsers brainfuck commands parsers mapped on its Brainfuck symbol.
     */
    BrainfuckExecutor(BrainfuckEnvironment environment,
                      Map<String, ICommand> commandParsers) {
        this.environment = environment;
        this.commandParsers = commandParsers;
    }

    /**
     * Executes Brainfuck code.
     */
    public void execute(String brainfuckCode) {
        for (int pointer = 0; pointer < brainfuckCode.length(); pointer++) {
            String commandSymbol = String.valueOf(brainfuckCode.charAt(pointer));
            ICommand commandParser = commandParsers.get(commandSymbol);
            if (commandParser == null) {
                throw new UnsupportedOperationException(String.format("There is no handler for command %s", commandSymbol));
            }
            pointer = commandParser.execute(environment, pointer);
        }
    }

    public void resetEnvironment() {
        environment.resetEnvironment();
    }
}
