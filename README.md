# Brainfuck Interpreter

Brainfuck interpreter written on Java.

### Quick Start

Brainfuck interpreter requires Java(7+) and Maven.

To try it navigate to the project directory and execute the following commands:
```sh
mvn clean package
java -jar target/brainfuck-interpreter-1.0.jar
```

### Note
To generate Brainfuck text you can use this [Brainfuck text generator](https://copy.sh/brainfuck/text.html).